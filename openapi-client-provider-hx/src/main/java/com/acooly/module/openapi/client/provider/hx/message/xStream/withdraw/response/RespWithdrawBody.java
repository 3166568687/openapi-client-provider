package com.acooly.module.openapi.client.provider.hx.message.xStream.withdraw.response;


import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import lombok.Data;

@Data
@XmlAccessorType(XmlAccessType.FIELD)
@XStreamAlias("Body")
public class RespWithdrawBody {

    /**
     *商户号
     */
    @XStreamAlias("MerCode")
    private String merCode;
    /**
     *商户 IPS 账户号
     */
    @XStreamAlias("merAccount")
    private String merAccount;
    /**
     *IPS 生产的批次委托付款凭证
     */
    @XStreamAlias("BatchBillno")
    private String batchBillno;
    /**
     *0：未处理，8：待审核，9：失败
     */
    @XStreamAlias("BatchStatus")
    private String batchStatus;
    /**
     *批次信息错误是返回
     */
    @XStreamAlias("BatchErrorMsg")
    private String batchErrorMsg;
    /**
     *明细列表，详见下表
     */
    @XStreamAlias("Details")
    private RespWithdrawDetails respWithdrawDetails;

}
