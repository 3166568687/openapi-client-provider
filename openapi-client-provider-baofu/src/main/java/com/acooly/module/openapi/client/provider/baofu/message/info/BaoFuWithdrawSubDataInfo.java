package com.acooly.module.openapi.client.provider.baofu.message.info;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author zhike 2018/2/2 17:13
 */
@Getter
@Setter
@XStreamAlias("trans_reqDatas")
public class BaoFuWithdrawSubDataInfo {

    /**
     * 批量提现响应报文
     */
    @XStreamImplicit(itemFieldName = "trans_reqData")
    private List<BaoFuWithdrawResponseInfo> withdrawResponseInfos;
}
