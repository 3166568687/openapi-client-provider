/*
 * ouwen@yiji.com Inc.
 * Copyright (c) 2017 All Rights Reserved.
 * create by zhike
 * date:2017-03-30
 *
 */
package com.acooly.module.openapi.client.provider.baofu.enums;

import com.acooly.core.utils.enums.Messageable;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * inst_channel_api DebitCreditType 枚举定义
 *
 * @author zhike Date: 2017-03-30 15:12:06
 */
public enum BaoFuBatchDeductBankCodeEnum implements Messageable {
    ICBC("ICBC", "中工商银行"),
    ABC("ABC", "中农业银行"),
    CCB("CCB", "中建设银行"),
    BOC("BOC", "中国银行"),
    BCOM("BCOM", "中交通银行"),
    CIB("CIB", "兴业银行"),
    CITIC("CITIC", "中信银行"),
    CEB("CEB", "中光大银行"),
    PAB("PAB", "平安银行"),
    PSBC("PSBC", "中国邮储蓄银行"),
    SHB("SHB", "上海银行"),
    SPDB("SPDB", "浦发展银行"),
    CMBC("CMBC", "民生银行"),
    CMB("CMB", "招商银行"),
    GDB("GDB", "广发银行"),
    HZB("HZB", "杭州银行"),
    BOB("BOB", "北京银行"),
    NBCB("NBCB", "宁波银行"),
    JSB("JSB", "江苏银行"),
    ZSB("ZSB", "浙商银行"),;

    private final String code;
    private final String message;

    private BaoFuBatchDeductBankCodeEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String code() {
        return code;
    }

    public String message() {
        return message;
    }

    public static Map<String, String> mapping() {
        Map<String, String> map = new LinkedHashMap<String, String>();
        for (BaoFuBatchDeductBankCodeEnum type : values()) {
            map.put(type.getCode(), type.getMessage());
        }
        return map;
    }

    /**
     * 通过枚举值码查找枚举值。
     *
     * @param code 查找枚举值的枚举值码。
     * @return 枚举值码对应的枚举值。
     * @throws IllegalArgumentException 如果 code 没有对应的 Status 。
     */
    public static BaoFuBatchDeductBankCodeEnum find(String code) {
        for (BaoFuBatchDeductBankCodeEnum status : values()) {
            if (status.getCode().equals(code)) {
                return status;
            }
        }
        return null;
    }

    /**
     * 获取全部枚举值。
     *
     * @return 全部枚举值。
     */
    public static List<BaoFuBatchDeductBankCodeEnum> getAll() {
        List<BaoFuBatchDeductBankCodeEnum> list = new ArrayList<BaoFuBatchDeductBankCodeEnum>();
        for (BaoFuBatchDeductBankCodeEnum status : values()) {
            list.add(status);
        }
        return list;
    }

    /**
     * 获取全部枚举值码。
     *
     * @return 全部枚举值码。
     */
    public static List<String> getAllCode() {
        List<String> list = new ArrayList<String>();
        for (BaoFuBatchDeductBankCodeEnum status : values()) {
            list.add(status.code());
        }
        return list;
    }
}
