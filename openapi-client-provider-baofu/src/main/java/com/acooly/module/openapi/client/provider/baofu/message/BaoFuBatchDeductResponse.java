package com.acooly.module.openapi.client.provider.baofu.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.baofu.domain.BaoFuApiMsgInfo;
import com.acooly.module.openapi.client.provider.baofu.domain.BaoFuResponse;
import com.acooly.module.openapi.client.provider.baofu.enums.BaoFuServiceEnum;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/1/29 16:12
 */
@Getter
@Setter
@BaoFuApiMsgInfo(service = BaoFuServiceEnum.BATCH_DEDUCT,type = ApiMessageType.Response)
@XStreamAlias("result")
public class BaoFuBatchDeductResponse extends BaoFuResponse{

    /**
     * 商户批次号 商户请求批次号，每次请求都不可重复
     *
     */
    @XStreamAlias("batch_id")
    private String batchId;
}
