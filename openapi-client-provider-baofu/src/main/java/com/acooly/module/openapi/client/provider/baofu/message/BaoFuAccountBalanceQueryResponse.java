package com.acooly.module.openapi.client.provider.baofu.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.baofu.domain.BaoFuApiMsgInfo;
import com.acooly.module.openapi.client.provider.baofu.domain.BaoFuResponse;
import com.acooly.module.openapi.client.provider.baofu.enums.BaoFuServiceEnum;
import com.acooly.module.openapi.client.provider.baofu.message.info.BaoFuAccountBalanceQueryTransReqDatas;
import com.acooly.module.openapi.client.provider.baofu.message.info.BaoFuReturnHead;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/3/1 17:33
 */
@Getter
@Setter
@XStreamAlias("trans_content")
@BaoFuApiMsgInfo(service = BaoFuServiceEnum.ACCOUNT_BALANCE_QUERY,type = ApiMessageType.Response)
public class BaoFuAccountBalanceQueryResponse extends BaoFuResponse {

    /**
     *响应报文头
     */
    @XStreamAlias("trans_head")
    private BaoFuReturnHead returnHead;

    /**
     * 响应报文体
     */
    @XStreamAlias("trans_reqDatas")
    private BaoFuAccountBalanceQueryTransReqDatas transReqDatas;
}
