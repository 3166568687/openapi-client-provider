package com.acooly.module.openapi.client.provider.wft.message;

import com.acooly.core.utils.Ids;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.wft.domain.WftApiMsgInfo;
import com.acooly.module.openapi.client.provider.wft.domain.WftRequest;
import com.acooly.module.openapi.client.provider.wft.enums.WftServiceEnum;
import com.acooly.module.openapi.client.provider.wft.support.WftAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

/**
 * @author zhike 2017/11/28 21:01
 */
@Getter
@Setter
@WftApiMsgInfo(service = WftServiceEnum.PAY_ALIPAY_NATIVE, type = ApiMessageType.Request)
public class WftPayAlipayNativeRequest extends WftRequest{
    /**
     * 订单号
     */
    @WftAlias(value = "out_trade_no")
    @NotBlank(message = "订单号不能为空")
    @Size(max = 32)
    private String outTradeNo;

    /**
     * 设备号
     */
    @WftAlias(value = "device_info")
    @Size(max = 32)
    private String deviceInfo;

    /**
     * 商品描述
     */
    @WftAlias(value = "body")
    @Size(max = 127)
    @NotBlank(message = "商品描述不能为空")
    private String body;

    /**
     * 附加信息
     */
    @WftAlias(value = "attach")
    private String attach;

    /**
     * 总金额，以分为单位，不允许包含任何字、符号
     */
    @WftAlias(value = "total_fee")
    @NotBlank(message = "金额不能为空")
    private String amount;

    /**
     * 终端IP，订单生成的机器 IP
     */
    @WftAlias(value = "mch_create_ip")
    @NotBlank(message = "终端IP不能为空")
    @Size(max = 16)
    private String mchCreateIp;

    /**
     * 接收平台通知的URL，需给绝对路径，255字符内格式如:http://wap.tenpay.com/tenpay.asp，确保平台能通过互联网访问该地址
     */
    @WftAlias(value = "notify_url")
    @NotBlank(message = "异步通知地址不能为空")
    @Size(max = 255)
    private String notifyUrl;

    /**
     * 订单生成时间 订单生成时间，格式为yyyymmddhhmmss，如2009年12月25日9点10分10秒表示为20091225091010。时区为GMT+8
     * beijing。该时间取自商户服务器。注：订单生成时间与超时时间需要同时传入才会生效。
     */
    @WftAlias(value = "time_start")
    @Size(max = 14)
    private String timeStart;

    /**
     * 订单超时时间 订单失效时间，格式为yyyymmddhhmmss，如2009年12月27日9点10分10秒表示为20091227091010。时区为GMT+8
     * beijing。该时间取自商户服务器。注：订单生成时间与超时时间需要同时传入才会生效。
     */
    @WftAlias(value = "time_expire")
    @Size(max = 14)
    private String timeExpire;

    /**
     * 订单最晚付款时间
     * 该笔订单允许的最晚付款时间，逾期将关闭交易，从下单开始计时。取值范围：1m~15d。m-分钟，h-小时，d-天，1c-当天(1c-当天的情况下，无论交易何时创建，都在0点关闭)。该参数数值不接受小数点，如1.5h，可转换为90m。
     */
    @WftAlias(value = "qr_code_timeout_express")
    @Size(max = 6)
    private String qrCodeTimeoutExpress;

    /**
     * 操作员 操作员帐号,默认为商户号
     */
    @WftAlias(value = "op_user_id")
    @Size(max = 32)
    private String opUserId;

    /**
     * 商品标记
     */
    @WftAlias(value = "goods_tag")
    @Size(max = 32)
    private String goodsTag;

    /**
     * 商品 ID
     */
    @WftAlias(value = "product_id")
    @Size(max = 32)
    private String productId;

    /**
     * 随机字符串
     */
    @WftAlias(value = "nonce_str")
    @Size(max = 32)
    private String nonceStr = Ids.oid();

    /**
     * 限定用户使用时能否使用信用卡，值为1，禁用信用卡；值为0或者不传此参数则不禁用
     */
    @WftAlias(value = "limit_credit_pay")
    @Size(max = 32)
    private String limitCreditPay = "0";
}
