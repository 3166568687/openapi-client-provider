package com.acooly.module.openapi.client.provider.wft.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.wft.domain.WftApiMsgInfo;
import com.acooly.module.openapi.client.provider.wft.domain.WftRequest;
import com.acooly.module.openapi.client.provider.wft.enums.WftServiceEnum;
import com.acooly.module.openapi.client.provider.wft.support.WftAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

/**
 * @author zhike 2017/11/28 20:08
 */
@Getter
@Setter
@WftApiMsgInfo(service = WftServiceEnum.UNIFIED_MICROPAY_REVERSE, type = ApiMessageType.Request)
public class WftUnifiedMicropayReverseRequest extends WftRequest{

    /**
     * 商户订单号
     */
    @WftAlias(value = "out_trade_no")
    @Size(max = 32)
    @NotBlank(message = "订单号不能为空")
    private String outTradeNo;

    /**
     * 订单号
     */
    @WftAlias(value = "nonce_str")
    @NotBlank(message = "随机字符串不能为空")
    @Size(max = 32)
    private String nonceStr;
}
