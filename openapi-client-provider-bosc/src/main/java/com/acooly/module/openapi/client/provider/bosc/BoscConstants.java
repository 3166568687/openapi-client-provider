/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-09-17 17:49 创建
 */
package com.acooly.module.openapi.client.provider.bosc;

import com.acooly.module.openapi.client.provider.bosc.enums.member.BoscUserRoleEnum;
import com.google.common.collect.Maps;

import java.util.Map;

/**
 * @author zhangpu 2017-09-17 17:49
 */
public class BoscConstants {

    /**
     * 提供方名称
     */
    public static final String PROVIDER_NAME = "bosc";

    /**
     * 核心报文字段
     */
    public static final String SERVICE_NAME = "serviceName";
    public static final String PLATFORM_NO = "platformNo";
    public static final String USER_DEVICE = "userDevice";
    public static final String KEY_SERIAL = "keySerial";
    public static final String SIGN = "sign";
    public static final String REQ_DATA = "reqData";
    public static final String RESP_DATA = "respData";
    public static final String RESPONSE_TYPE = "ResponseType";
    
    /**
     * 查询接口服务名
     */
    public static final String QUERY_SERVICE_NAME = "QUERY_TRANSACTION";
    
    /**
     * 用户角色授权映射
     */
    public static final Map<BoscUserRoleEnum,String> ROLE_AUTH_MAPPER = Maps.newHashMap ();
    
    static {
        ROLE_AUTH_MAPPER.put (BoscUserRoleEnum.INVESTOR,"TENDER,CREDIT_ASSIGNMENT");
        ROLE_AUTH_MAPPER.put (BoscUserRoleEnum.BORROWERS,"REPAYMENT,WITHDRAW,RECHARGE");
        ROLE_AUTH_MAPPER.put (BoscUserRoleEnum.COLLABORATOR,"COMPENSATORY,CREDIT_ASSIGNMENT");
        ROLE_AUTH_MAPPER.put (BoscUserRoleEnum.GUARANTEECORP,"COMPENSATORY,CREDIT_ASSIGNMENT");
        ROLE_AUTH_MAPPER.put (BoscUserRoleEnum.SUPPLIER,"COMPENSATORY,CREDIT_ASSIGNMENT");
    }
    
    
    /**
     * 自动授权列表
     */
    public static final String AUTH_LIST = "TENDER,REPAYMENT,CREDIT_ASSIGNMENT,COMPENSATORY,WITHDRAW,RECHARGE";
    
    
    
}
