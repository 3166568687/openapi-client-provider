package com.acooly.module.openapi.client.provider.bosc.message.fund;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.bosc.domain.ApiMsgInfo;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscRequest;
import com.acooly.module.openapi.client.provider.bosc.enums.BoscServiceNameEnum;
import com.acooly.module.openapi.client.provider.bosc.enums.trade.BoscBizTypeEnum;
import com.acooly.module.openapi.client.provider.bosc.message.fund.info.BizDetailInfo;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.util.Assert;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

/**
 * 下载对账文件
 * Created by liubin@prosysoft.com on 2017/10/10.
 */
@ApiMsgInfo(service = BoscServiceNameEnum.DOWNLOAD_CHECKFILE, type = ApiMessageType.Request)
public class DownloadCheckFileRequest extends BoscRequest {
	
	@NotBlank(message = "对账文件日期不能为空")
	private String fileDate;
	
	@JSONField(serialize = false)
	@NotBlank(message = "文件存放本地路径不能为空")
	private String filePath;
	
	public DownloadCheckFileRequest () {
		setService (BoscServiceNameEnum.DOWNLOAD_CHECKFILE.code ());
	}
	
	public String getFileDate () {
		return fileDate;
	}
	
	public void setFileDate (String fileDate) {
		this.fileDate = fileDate;
	}
	
	public String getFilePath () {
		return filePath;
	}
	
	public void setFilePath (String filePath) {
		this.filePath = filePath;
	}
}
