package com.acooly.module.openapi.client.provider.bosc.message.member;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.bosc.BoscConstants;
import com.acooly.module.openapi.client.provider.bosc.domain.ApiMsgInfo;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscRequest;
import com.acooly.module.openapi.client.provider.bosc.enums.BoscServiceNameEnum;
import com.acooly.module.openapi.client.provider.bosc.enums.fund.BoscBankcodeEnum;
import com.acooly.module.openapi.client.provider.bosc.enums.member.BoscIdCardTypeEnum;
import com.acooly.module.openapi.client.provider.bosc.enums.member.BoscUserRoleEnum;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@ApiMsgInfo(service = BoscServiceNameEnum.ENTERPRISE_REGISTER,type = ApiMessageType.Request)
public class EnterpriseRegisterRequest extends BoscRequest {
	/**
	 * 请求流水号
	 */
	@NotEmpty
	@Size(max = 50)
	private String requestNo;
	/**
	 * 平台用户编号
	 */
	@NotEmpty
	@Size(max = 50)
	private String platformUserNo;
	/**
	 * 企业名称
	 */
	@NotEmpty
	@Size(max = 50)
	private String enterpriseName;
	/**
	 * 开户银行许可证号
	 */
	@NotEmpty
	@Size(max = 50)
	private String bankLicense;
	/**
	 * 组织机构代码
	 */
	@Size(max = 50)
	private String orgNo;
	/**
	 * 营业执照编号
	 */
	@Size(max = 50)
	private String businessLicense;
	/**
	 * 税务登记号
	 */
	@Size(max = 50)
	private String taxNo;
	/**
	 * 统一社会信用代码（可替代税务登记号、税务登记号、组织机构代码此三证），统 一社会信用代码和三证信息两者必须有一个进行传入
	 */
	@Size(max = 50)
	private String unifiedCode;
	/**
	 * 机构信用代码
	 */
	@Size(max = 50)
	private String creditCode;
	/**
	 * 法人姓名
	 */
	@NotEmpty
	@Size(max = 50)
	private String legal;
	/**
	 * 法人证件类型
	 */
	@NotNull
	private BoscIdCardTypeEnum idCardType;
	/**
	 * 法人证件号
	 */
	@NotEmpty
	@Size(max = 50)
	private String legalIdCardNo;
	/**
	 * 企业联系人
	 */
	@NotEmpty
	@Size(max = 50)
	private String contact;
	/**
	 * 联系人手机号
	 */
	@NotEmpty
	@Size(max = 50)
	private String contactPhone;
	/**
	 * 见【用户角色】
	 */
	@NotNull
	private BoscUserRoleEnum userRole;
	/**
	 * 企业对公账号
	 */
	@NotEmpty
	@Size(max = 50)
	private String bankcardNo;
	/**
	 * 见【银行编码】
	 */
	@NotNull
	private BoscBankcodeEnum bankcode;
	/**
	 * 页面回跳 URL
	 */
	@NotEmpty
	@Size(max = 100)
	private String redirectUrl;
	/**
	 * 见【用户授权列表】；此处可传多个值，传多个值用“,”英文半角逗号分隔
	 */
	private String authList = BoscConstants.AUTH_LIST;
	
	public EnterpriseRegisterRequest () {
		setService (BoscServiceNameEnum.ENTERPRISE_REGISTER.code ());
	}
	
	public EnterpriseRegisterRequest (String platformUserNo,
	                                  BoscUserRoleEnum userRole, String redirectUrl, String requestNo) {
		this ();
		this.platformUserNo = platformUserNo;
		this.userRole = userRole;
		this.redirectUrl = redirectUrl;
		this.requestNo = requestNo;
		this.authList = BoscConstants.ROLE_AUTH_MAPPER.get (userRole);
	}
	
	public EnterpriseRegisterRequest (String requestNo, String platformUserNo, String enterpriseName,
	                                  String bankLicense, String orgNo, String businessLicense, String taxNo,
	                                  String unifiedCode, String creditCode, String legal,
	                                  BoscIdCardTypeEnum idCardType, String legalIdCardNo, String contact,
	                                  String contactPhone,
	                                  BoscUserRoleEnum userRole, String bankcardNo,
	                                  BoscBankcodeEnum bankcode, String redirectUrl) {
		
		this (platformUserNo, userRole, redirectUrl, requestNo);
		this.enterpriseName = enterpriseName;
		this.bankLicense = bankLicense;
		this.orgNo = orgNo;
		this.businessLicense = businessLicense;
		this.taxNo = taxNo;
		this.unifiedCode = unifiedCode;
		this.creditCode = creditCode;
		this.legal = legal;
		this.idCardType = idCardType;
		this.legalIdCardNo = legalIdCardNo;
		this.contact = contact;
		this.contactPhone = contactPhone;
		this.bankcardNo = bankcardNo;
		this.bankcode = bankcode;
	}
	
	public String getRequestNo () {
		return requestNo;
	}
	
	public void setRequestNo (String requestNo) {
		this.requestNo = requestNo;
	}
	
	public String getPlatformUserNo () {
		return platformUserNo;
	}
	
	public void setPlatformUserNo (String platformUserNo) {
		this.platformUserNo = platformUserNo;
	}
	
	public String getEnterpriseName () {
		return enterpriseName;
	}
	
	public void setEnterpriseName (String enterpriseName) {
		this.enterpriseName = enterpriseName;
	}
	
	public String getBankLicense () {
		return bankLicense;
	}
	
	public void setBankLicense (String bankLicense) {
		this.bankLicense = bankLicense;
	}
	
	public String getOrgNo () {
		return orgNo;
	}
	
	public void setOrgNo (String orgNo) {
		this.orgNo = orgNo;
	}
	
	public String getBusinessLicense () {
		return businessLicense;
	}
	
	public void setBusinessLicense (String businessLicense) {
		this.businessLicense = businessLicense;
	}
	
	public String getTaxNo () {
		return taxNo;
	}
	
	public void setTaxNo (String taxNo) {
		this.taxNo = taxNo;
	}
	
	public String getUnifiedCode () {
		return unifiedCode;
	}
	
	public void setUnifiedCode (String unifiedCode) {
		this.unifiedCode = unifiedCode;
	}
	
	public String getCreditCode () {
		return creditCode;
	}
	
	public void setCreditCode (String creditCode) {
		this.creditCode = creditCode;
	}
	
	public String getLegal () {
		return legal;
	}
	
	public void setLegal (String legal) {
		this.legal = legal;
	}
	
	public BoscIdCardTypeEnum getIdCardType () {
		return idCardType;
	}
	
	public void setIdCardType (BoscIdCardTypeEnum idCardType) {
		this.idCardType = idCardType;
	}
	
	public String getLegalIdCardNo () {
		return legalIdCardNo;
	}
	
	public void setLegalIdCardNo (String legalIdCardNo) {
		this.legalIdCardNo = legalIdCardNo;
	}
	
	public String getContact () {
		return contact;
	}
	
	public void setContact (String contact) {
		this.contact = contact;
	}
	
	public String getContactPhone () {
		return contactPhone;
	}
	
	public void setContactPhone (String contactPhone) {
		this.contactPhone = contactPhone;
	}
	
	public BoscUserRoleEnum getUserRole () {
		return userRole;
	}
	
	public void setUserRole (BoscUserRoleEnum userRole) {
		this.userRole = userRole;
		this.authList = BoscConstants.ROLE_AUTH_MAPPER.get (userRole);
	}
	
	public String getBankcardNo () {
		return bankcardNo;
	}
	
	public void setBankcardNo (String bankcardNo) {
		this.bankcardNo = bankcardNo;
	}
	
	public BoscBankcodeEnum getBankcode () {
		return bankcode;
	}
	
	public void setBankcode (BoscBankcodeEnum bankcode) {
		this.bankcode = bankcode;
	}
	
	public String getRedirectUrl () {
		return redirectUrl;
	}
	
	public void setRedirectUrl (String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}
	
	public String getAuthList () {
		return authList;
	}
	
	public void setAuthList (String authList) {
		this.authList = authList;
	}
}