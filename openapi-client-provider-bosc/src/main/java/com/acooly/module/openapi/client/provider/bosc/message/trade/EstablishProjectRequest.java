package com.acooly.module.openapi.client.provider.bosc.message.trade;

import com.acooly.core.utils.Money;
import com.acooly.core.utils.validate.jsr303.MoneyConstraint;
import com.acooly.module.openapi.client.provider.bosc.domain.ApiMsgInfo;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.bosc.enums.BoscServiceNameEnum;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscRequest;
import com.acooly.module.openapi.client.provider.bosc.enums.trade.BoscProjectTypeEnum;
import com.acooly.module.openapi.client.provider.bosc.enums.trade.BoscRepaymentWayEnum;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@ApiMsgInfo(service = BoscServiceNameEnum.ESTABLISH_PROJECT, type = ApiMessageType.Request)
public class EstablishProjectRequest extends BoscRequest {
	/**
	 * 请求流水号
	 */
	@NotEmpty
	@Size(max = 50)
	private String requestNo;
	/**
	 * 借款方平台用户编号
	 */
	@NotEmpty
	@Size(max = 50)
	private String platformUserNo;
	/**
	 * 标的号
	 */
	@NotEmpty
	@Size(max = 50)
	private String projectNo;
	/**
	 * 标的金额
	 */
	@MoneyConstraint(min = 1)
	private Money projectAmount;
	/**
	 * 标的名称
	 */
	@NotEmpty
	@Size(max = 300)
	private String projectName;
	/**
	 * 标的描述
	 */
	@Size(max = 300)
	private String projectDescription;
	/**
	 * 见【标的类型】
	 */
	@NotNull
	private BoscProjectTypeEnum projectType = BoscProjectTypeEnum.STANDARDPOWDER;
	/**
	 * 标的期限（单位：天）（只做记录，不做严格校验）
	 */
	private int projectPeriod;
	/**
	 * 年化利率（只做记录，不做严格校验）  14% 的话 传递 0.14
	 */
	@NotEmpty
	private String annnualInterestRate;
	/**
	 * 见【还款方式】（只做记录，不做严格校验）
	 */
	@NotNull
	private BoscRepaymentWayEnum repaymentWay;
	/**
	 * 标的扩展信息
	 */
	private String extend;
	
	public EstablishProjectRequest () {
		setService (BoscServiceNameEnum.ESTABLISH_PROJECT.code ());
	}
	
	public EstablishProjectRequest (String requestNo, String platformUserNo, String projectNo,
	                                Money projectAmount, String projectName) {
		this();
		this.requestNo = requestNo;
		this.platformUserNo = platformUserNo;
		this.projectNo = projectNo;
		this.projectAmount = projectAmount;
		this.projectName = projectName;
	}
	
	public String getRequestNo () {
		return requestNo;
	}
	
	public void setRequestNo (String requestNo) {
		this.requestNo = requestNo;
	}
	
	public String getPlatformUserNo () {
		return platformUserNo;
	}
	
	public void setPlatformUserNo (String platformUserNo) {
		this.platformUserNo = platformUserNo;
	}
	
	public String getProjectNo () {
		return projectNo;
	}
	
	public void setProjectNo (String projectNo) {
		this.projectNo = projectNo;
	}
	
	public Money getProjectAmount () {
		return projectAmount;
	}
	
	public void setProjectAmount (Money projectAmount) {
		this.projectAmount = projectAmount;
	}
	
	public String getProjectName () {
		return projectName;
	}
	
	public void setProjectName (String projectName) {
		this.projectName = projectName;
	}
	
	public String getProjectDescription () {
		return projectDescription;
	}
	
	public void setProjectDescription (String projectDescription) {
		this.projectDescription = projectDescription;
	}
	
	public BoscProjectTypeEnum getProjectType () {
		return projectType;
	}
	
	public void setProjectType (BoscProjectTypeEnum projectType) {
		this.projectType = projectType;
	}
	
	public int getProjectPeriod () {
		return projectPeriod;
	}
	
	public void setProjectPeriod (int projectPeriod) {
		this.projectPeriod = projectPeriod;
	}
	
	public String getAnnnualInterestRate () {
		return annnualInterestRate;
	}
	
	public void setAnnnualInterestRate (String annnualInterestRate) {
		this.annnualInterestRate = annnualInterestRate;
	}
	
	public BoscRepaymentWayEnum getRepaymentWay () {
		return repaymentWay;
	}
	
	public void setRepaymentWay (BoscRepaymentWayEnum repaymentWay) {
		this.repaymentWay = repaymentWay;
	}
	
	public String getExtend () {
		return extend;
	}
	
	public void setExtend (String extend) {
		this.extend = extend;
	}
}