package com.acooly.module.openapi.client.provider.bosc.message.fund;

import com.acooly.core.utils.Money;
import com.acooly.core.utils.validate.jsr303.MoneyConstraint;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.bosc.domain.ApiMsgInfo;
import com.acooly.module.openapi.client.provider.bosc.enums.BoscServiceNameEnum;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscRequest;
import com.acooly.module.openapi.client.provider.bosc.enums.fund.BoscWithdrawFormEnum;
import com.acooly.module.openapi.client.provider.bosc.enums.fund.BoscWithdrawTypeEnum;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@ApiMsgInfo(service = BoscServiceNameEnum.WITHDRAW, type = ApiMessageType.Request)
public class WithdrawRequest extends BoscRequest {
	/**
	 * 平台用户编号
	 */
	@NotEmpty
	@Size(max = 50)
	private String platformUserNo;
	/**
	 * 请求流水号
	 */
	@NotEmpty
	@Size(max = 50)
	private String requestNo;
	/**
	 * 超过此时间即页面过期  格式yyyyMMddHHmmss
	 */
	@NotNull
	private Date expired;
	/**
	 * 页面回跳 URL
	 */
	@NotEmpty
	@Size(max = 100)
	private String redirectUrl;
	/**
	 * 见【提现方式】，NORMAL 表示普通 T1，URGENT 表示加急 T0，NORMAL_URGENT
	 */
	@NotNull
	private BoscWithdrawTypeEnum withdrawType = BoscWithdrawTypeEnum.NORMAL;
	/**
	 * 提现类型，IMMEDIATE 为直接提现，CONFIRMED 为待确认提现，不传默认为直 接提现方式；仅直接提现支持加急 T0 或智能 T0；
	 */
	private BoscWithdrawFormEnum withdrawForm = BoscWithdrawFormEnum.CONFIRMED;
	/**
	 * 提现金额
	 */
	@MoneyConstraint(min = 1)
	private Money amount;
	/**
	 * 提现分佣
	 */
	@MoneyConstraint(nullable = true)
	private Money commission;
	
	public WithdrawRequest () {
		setService (BoscServiceNameEnum.WITHDRAW.code ());
	}
	
	public WithdrawRequest (String platformUserNo, String requestNo, Date expired, String redirectUrl,Money amount, Money commission) {
		this();
		this.platformUserNo = platformUserNo;
		this.requestNo = requestNo;
		this.expired = expired;
		this.redirectUrl = redirectUrl;
		this.amount = amount;
		this.commission = commission;
	}
	
	public String getPlatformUserNo () {
		return platformUserNo;
	}
	
	public void setPlatformUserNo (String platformUserNo) {
		this.platformUserNo = platformUserNo;
	}
	
	public String getRequestNo () {
		return requestNo;
	}
	
	public void setRequestNo (String requestNo) {
		this.requestNo = requestNo;
	}
	
	
	public Date getExpired () {
		return expired;
	}
	
	public void setExpired (Date expired) {
		this.expired = expired;
	}
	
	public String getRedirectUrl () {
		return redirectUrl;
	}
	
	public void setRedirectUrl (String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}
	
	public BoscWithdrawTypeEnum getWithdrawType () {
		return withdrawType;
	}
	
	public void setWithdrawType (BoscWithdrawTypeEnum withdrawType) {
		this.withdrawType = withdrawType;
	}
	
	public BoscWithdrawFormEnum getWithdrawForm () {
		return withdrawForm;
	}
	
	public void setWithdrawForm (BoscWithdrawFormEnum withdrawForm) {
		this.withdrawForm = withdrawForm;
	}
	
	public Money getAmount () {
		return amount;
	}
	
	public void setAmount (Money amount) {
		this.amount = amount;
	}
	
	public Money getCommission () {
		return commission;
	}
	
	public void setCommission (Money commission) {
		this.commission = commission;
	}
}