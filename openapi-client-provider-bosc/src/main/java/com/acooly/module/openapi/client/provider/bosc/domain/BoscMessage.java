/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-09-22 09:22 创建
 */
package com.acooly.module.openapi.client.provider.bosc.domain;

import com.acooly.core.utils.ToString;
import com.acooly.module.openapi.client.api.message.ApiMessage;
import com.alibaba.fastjson.annotation.JSONField;

import javax.persistence.Transient;

/**
 * @author zhangpu 2017-09-22 09:22
 */
public class BoscMessage implements ApiMessage {

    @Transient
    @JSONField(serialize = false)
    private String service;
    @Transient
    @JSONField(serialize = false)
    private String partner;
    @Transient
    @JSONField(serialize = false)
    private String serialkey = "1";
    @Transient
    @JSONField(serialize = false)
    private String sign;

    public void doCheck() {

    }

    @Override
    public String getService() {
        return this.service;
    }

    @Override
    public String getPartner() {
        return this.partner;
    }

    public void setService(String service) {
        this.service = service;
    }

    public void setPartner(String partner) {
        this.partner = partner;
    }

    public String getSerialkey() {
        return serialkey;
    }

    public void setSerialkey(String serialkey) {
        this.serialkey = serialkey;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }


    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
