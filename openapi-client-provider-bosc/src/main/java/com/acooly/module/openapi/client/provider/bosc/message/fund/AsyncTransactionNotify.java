package com.acooly.module.openapi.client.provider.bosc.message.fund;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.api.message.ApiMessage;
import com.acooly.module.openapi.client.provider.bosc.domain.ApiMsgInfo;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscMessage;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscNotify;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscRequest;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscResponse;
import com.acooly.module.openapi.client.provider.bosc.enums.BoscServiceNameEnum;
import com.acooly.module.openapi.client.provider.bosc.message.fund.info.BizDetailInfo;
import com.acooly.module.openapi.client.provider.bosc.message.fund.info.NotifyDetailInfo;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.List;

/**
 * Created by liubin@prosysoft.com on 2017/10/10.
 */
@ApiMsgInfo(service = BoscServiceNameEnum.ASYNC_TRANSACTION, type = ApiMessageType.Notify)
public class AsyncTransactionNotify extends BoscNotify {
	
	@NotEmpty(message = "异步通知交易明细")
	private List<NotifyDetailInfo> details;
	
	public List<NotifyDetailInfo> getDetails () {
		return details;
	}
	
	public void setDetails (
			List<NotifyDetailInfo> details) {
		this.details = details;
	}
}
