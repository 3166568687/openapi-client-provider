package com.acooly.module.openapi.client.provider.bosc.message.fund;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.bosc.domain.ApiMsgInfo;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscMessage;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscResponse;
import com.acooly.module.openapi.client.provider.bosc.enums.BoscServiceNameEnum;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * Created by liubin@prosysoft.com on 2017/10/10.
 */
@ApiMsgInfo(service = BoscServiceNameEnum.ASYNC_TRANSACTION, type = ApiMessageType.Response)
public class AsyncTransactionResponse extends BoscResponse {
	
	/**
	 * 批量交易批次号
	 */
	@NotEmpty
	@Size(max = 50)
	private String requestNo;
	
	public String getRequestNo () {
		return requestNo;
	}
	
	public void setRequestNo (String requestNo) {
		this.requestNo = requestNo;
	}
}
