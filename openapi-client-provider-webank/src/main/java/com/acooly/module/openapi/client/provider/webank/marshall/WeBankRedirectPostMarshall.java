package com.acooly.module.openapi.client.provider.webank.marshall;

import com.acooly.core.utils.Strings;
import com.acooly.module.openapi.client.api.marshal.ApiMarshal;
import com.acooly.module.openapi.client.api.message.PostRedirect;
import com.acooly.module.openapi.client.provider.webank.domain.WeBankRequest;

import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;


@Service
@Slf4j
public class WeBankRedirectPostMarshall extends WeBankMarshallSupport implements ApiMarshal<PostRedirect, WeBankRequest> {

    @Override
    public PostRedirect marshal(WeBankRequest source) {
        PostRedirect postRedirect = new PostRedirect();
        if(Strings.isBlank(source.getGatewayUrl())) {
            source.setGatewayUrl(getProperties().getGatewayUrl());
        }
        if(Strings.isBlank(source.getPartnerId())) {
            source.setPartnerId(getProperties().getPartnerId());
        }
        postRedirect.setRedirectUrl(source.getGatewayUrl());
        //postRedirect.setFormDatas(doMarshall(source));
        log.info("跳转报文: {}", postRedirect);
        return postRedirect;
    }

}
