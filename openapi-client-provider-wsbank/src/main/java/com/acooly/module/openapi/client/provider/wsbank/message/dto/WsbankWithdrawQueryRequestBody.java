package com.acooly.module.openapi.client.provider.wsbank.message.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;
import java.io.Serializable;

@Getter
@Setter
@XStreamAlias("body")
public class WsbankWithdrawQueryRequestBody implements Serializable {

    /**
     * 合作方机构号（网商银行分配）
     */
    @Size(max = 32)
    @XStreamAlias("IsvOrgId")
    @NotBlank
    private String isvOrgId;

    /**
     * 提现商户号
     */
    @Size(max = 32)
    @XStreamAlias("MerchantId")
    @NotBlank
    private String merchantId;

    /**
     * 外部交易号。合作方系统生成的外部交易号，同一交易号被视为同一笔交易
     */
    @Size(max = 64)
    @XStreamAlias("OutTradeNo")
    @NotBlank
    private String outTradeNo;

    /**
     * 网商提现订单号
     */
    @Size(max = 64)
    @XStreamAlias("OrderNo")
    private String orderNo;
}
