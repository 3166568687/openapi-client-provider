package com.acooly.module.openapi.client.provider.wsbank.message.dto;

import com.acooly.module.openapi.client.provider.wsbank.message.base.WsbankHeadResponse;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@XStreamAlias("response")
public class WsbankOrderShareResponseInfo implements Serializable {

    /**
     *响应报文头
     */
    @XStreamAlias("head")
    private WsbankHeadResponse headResponse;

    @XStreamAlias("body")
    private WsbankOrderShareResponseBody wsbankOrderShareResponseBody;

}
