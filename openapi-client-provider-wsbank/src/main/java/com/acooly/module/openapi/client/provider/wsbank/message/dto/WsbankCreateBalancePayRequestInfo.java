package com.acooly.module.openapi.client.provider.wsbank.message.dto;

import com.acooly.module.openapi.client.provider.wsbank.message.base.WsbankHeadRequest;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@XStreamAlias("request")
public class WsbankCreateBalancePayRequestInfo implements Serializable {

	private static final long serialVersionUID = -3571823733722184697L;

	/**
     * 请求报文头
     */
    @XStreamAlias("head")
    private WsbankHeadRequest headRequest;

    @XStreamAlias("body")
    private WsbankCreateBalancePayRequestBody wsbankCreateBalancePayRequestBody;


}
