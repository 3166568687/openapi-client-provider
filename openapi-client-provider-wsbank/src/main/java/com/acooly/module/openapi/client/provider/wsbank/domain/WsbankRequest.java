/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhike
 * date:2016年3月31日
 *
 */
package com.acooly.module.openapi.client.provider.wsbank.domain;

import lombok.Getter;
import lombok.Setter;

/** @author zhike */
@Getter
@Setter
public class WsbankRequest extends WsbankApiMessage {

}
