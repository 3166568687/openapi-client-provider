package com.acooly.module.openapi.client.provider.wsbank.message.dto;

import com.acooly.module.openapi.client.provider.wsbank.message.base.WsbankResponseInfo;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author zhike 2018/5/23 9:56
 */
@Getter
@Setter
@XStreamAlias("body")
public class WsbankUploadImgResponseBody implements Serializable {

    /**
     * 返回码组件。当ResultStatus=S时才有后续的参数返回。
     */
    @XStreamAlias("RespInfo")
    private WsbankResponseInfo responseInfo;

    /**
     *外部交易号
     */
    @XStreamAlias("OutTradeNo")
    private String outTradeNo;

    /**
     * 文件唯一编号，非地址，公网不可访问
     */
    @XStreamAlias("PhotoUrl")
    private String photoUrl;
}
