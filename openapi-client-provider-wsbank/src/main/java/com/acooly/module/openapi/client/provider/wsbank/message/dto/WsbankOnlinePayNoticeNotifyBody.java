package com.acooly.module.openapi.client.provider.wsbank.message.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author zhike 2018/5/24 14:42
 */
@Getter
@Setter
@XStreamAlias("body")
public class WsbankOnlinePayNoticeNotifyBody implements Serializable {

    /**
     * 外部交易号。由合作方系统生成。
     */
    @XStreamAlias("OutTradeNo")
    private String outTradeNo;

    /** 支付渠道类型。该笔支付走的第三方支付渠道。可选值： ALI：支付宝 WX：微信支付 QQ：手机QQ（暂未开放） JD：京东钱包（暂未开放） */
    @XStreamAlias("ChannelType")
    private String channelType;

    /**
     * 交易总额度。货币最小单位，人民币：分
     */
    @XStreamAlias("TotalAmount")
    private String totalAmount;

    /**
     * 币种。默认CNY。
     */
    @XStreamAlias("Currency")
    private String currency;

    /**
     * 商户号。网商为商户分配的商户号，通过商户入驻结果查询接口获取。
     */
    @XStreamAlias("MerchantId")
    private String merchantId;

    /**
     * 合作方机构号（网商银行分配）
     */
    @XStreamAlias("IsvOrgId")
    private String isvOrgId;

    /**
     *附加信息，原样返回
     */
    @XStreamAlias("Attach")
    private String attach;

    /**
     * 支付完成时间
     */
    @XStreamAlias("GmtPayment")
    private String gmtPayment;

    /**
     * 付款银行。银行类型，仅使用微信支付时有返回值。
     */
    @XStreamAlias("BankType")
    private String bankType;

    /** 用户是否关注商户公众号，仅微信支付有效。 Y：关注； N：未关注 */
    @XStreamAlias("IsSubscribe")
    private String isSubscribe;

    /**
     * 支付宝或微信端的订单号，可用于打印小票给客户核对
     */
    @XStreamAlias("PayChannelOrderNo")
    private String payChannelOrderNo;

    /**
     * 商户订单号。该订单号与支付宝、微信支付客户端账单详情显示的商户订单号一致，通过该订单后可发起扫码退款或查询交易。
     */
    @XStreamAlias("MerchantOrderNo")
    private String merchantOrderNo;

    /**
     * 子商户appid。仅微信返回。
     */
    @XStreamAlias("SubAppId")
    private String subAppId;

    /**
     * 现金券金额。仅微信返回，现金券支付金额<=订单总金额，订单总金额-现金券金额为现金支付金额。
     */
    @XStreamAlias("CouponFee")
    private String couponFee;

    /**
     * 用户在银行 appid 下的唯一标识。仅微信返回。
     */
    @XStreamAlias("OpenId")
    private String openId;

    /**
     * 用户在商户 appid下的唯一标识。若商户使用合作机构的appid，则返回合作机构下的用户唯一标识。
     */
    @XStreamAlias("SubOpenId")
    private String subOpenId;

    /**
     * 买家支付宝登录账号。
     */
    @XStreamAlias("BuyerLogonId")
    private String buyerLogonId;

    /**
     * 买家支付宝用户id。
     */
    @XStreamAlias("BuyerUserId")
    private String buyerUserId;

    /** 借贷标识。可选值： credit：信用卡 pcredit：花呗（仅支付宝） debit：借记卡 balance：余额 unknown：未知 */
    @XStreamAlias("Credit")
    private String credit;

    /**
     * 实收金额，商户实际入账的金额（扣手续费之前）。仅支付宝返回
     */
    @XStreamAlias("ReceiptAmount")
    private String receiptAmount;

    /**
     * 用户实付金额，建议打印在小票上避免退款时出现纠纷。仅支付宝返回
     */
    @XStreamAlias("BuyerPayAmount")
    private String buyerPayAmount;

    /**
     * 开票金额，快速告知商户应该给用户开多少钱发票。仅支付宝返回
     */
    @XStreamAlias("InvoiceAmount")
    private String invoiceAmount;
}
