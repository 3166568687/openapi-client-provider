package com.acooly.module.openapi.client.provider.wsbank.message.dto;

import com.acooly.module.openapi.client.provider.wsbank.message.base.WsbankHeadResponse;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author zhike 2018/5/22 22:08
 */
@Getter
@Setter
@XStreamAlias("response")
public class WsbankPayQueryResponseInfo implements Serializable {

    /**
     *响应报文头
     */
    @XStreamAlias("head")
    private WsbankHeadResponse headResponse;

    /**
     * 响应报文体
     */
    @XStreamAlias("body")
    private WsbankPayQueryResponseBody responseBody;
}
