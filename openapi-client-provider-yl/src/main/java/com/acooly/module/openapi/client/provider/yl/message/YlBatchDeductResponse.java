package com.acooly.module.openapi.client.provider.yl.message;


import com.acooly.core.utils.Money;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.yl.domain.YlApiMsgInfo;
import com.acooly.module.openapi.client.provider.yl.domain.YlResponse;
import com.acooly.module.openapi.client.provider.yl.enums.YlServiceEnum;
import com.acooly.module.openapi.client.provider.yl.message.dto.YlBatchRetInfo;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@YlApiMsgInfo(service = YlServiceEnum.YL_BATCH_DEDUCT, type = ApiMessageType.Response)
public class YlBatchDeductResponse extends YlResponse {

    /**
     * 交易类型
     */
    String bizType;

    /**
     *同步成功时返回实际金额
     */
    private Money amountLn;

    /**
     * 订单效果结果集合
     */
    private List<YlBatchRetInfo> batchRetDetils;

}
