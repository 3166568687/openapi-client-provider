/*
 * www.acooly.cn Inc.
 * Copyright (c) 2018 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2018-02-22 04:14:52 创建
 */package com.acooly.module.openapi.client.provider.fudian.message.fund;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.fudian.domain.FudianApiMsg;
import com.acooly.module.openapi.client.provider.fudian.domain.FudianNotify;
import com.acooly.module.openapi.client.provider.fudian.enums.FudianServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author zhangpu 2018-02-22 04:14:52
 */
@Getter
@Setter
@FudianApiMsg(service = FudianServiceNameEnum.ACCOUNT_WITHDRAW ,type = ApiMessageType.Notify)
public class AccountWithdrawNotify extends FudianNotify {

    /**
     * 账户号
     * 用户在本系统的唯一账户编号，由本系统生成.
     */
    @NotEmpty
    @Length(max=50)
    private String accountNo;

    /**
     * 提现金额
     * 提现金额，以元为单位，保留小数点后2位
     */
    @NotEmpty
    @Length(max=20)
    private String amount;

    /**
     * 提现银行卡号
     * 提现银行卡号
     */
    @NotEmpty
    @Length(max=256)
    private String bankCardNo;

    /**
     * 银行编码
     * 银行编码，详见附录【4.4】
     */
    @NotEmpty
    @Length(max=50)
    private String bankCode;

    /**
     * 提现银行名称
     * 提现银行名称
     */
    @NotEmpty
    @Length(max=256)
    private String bankName;

    /**
     * 提现手续费
     * 提现手续费，默认向用户收取以元为单位，保留小数点后2位
     */
    @NotEmpty
    @Length(max=20)
    private String fee;

    /**
     * 商户号
     * 用于校验主体参数和业务参数一致性，保证参数的安全传输
     */
    @NotEmpty
    @Length(max=8)
    private String merchantNo;

    /**
     * 银行卡实到金额
     * 当费用个人承担，银行卡到账金额等于提现金额减去费用
     */
    @NotEmpty
    @Length(max=20)
    private String receivedAmount;

    /**
     * 交易状态
     * 0:申请提现,1:提现成功,2:提现失败,3:银行退单,4:提现异常
     */
    @NotEmpty
    @Length(min = 1,max=1)
    private String status;

    /**
     * 用户名
     * 用户在本系统的唯一标识，由本系统生成
     */
    @NotEmpty
    @Length(max=32)
    private String userName;

    /**
     * 用户真实姓名
     * 用户真实姓名
     */
    @NotEmpty
    @Length(max=64)
    private String realName;
}