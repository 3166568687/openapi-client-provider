/*
 *
 * www.cutebear.com Inc.
 * Copyright (c) 2017  All Rights Reserved
 */

package com.acooly.module.openapi.client.provider.cmb.enums;

import com.google.common.collect.Maps;

import com.acooly.core.utils.enums.Messageable;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by liubin@prosysoft.com on 2017/9/27.
 */
public enum CmbStatusEnum implements Messageable {

	/**
	 * 同步响应失败
	 */
	INIT("INIT","失败"),

	/**
	 * 异步通知失败
	 */
	FAIL("FAIL","失败"),


	SUCCESS("SUCCESS","成功"),
	;


	private final String code;
	private final String message;

	private CmbStatusEnum(String code, String message) {
		this.code = code;
		this.message = message;
	}
	
	public String getCode() {
		return code;
	}
	
	public String getMessage() {
		return message;
	}
	
	public String code() {
		return code;
	}
	
	public String message() {
		return message;
	}
	
	public static Map<String, String> mapping() {
		Map<String, String> map = Maps.newLinkedHashMap();
		for (CmbStatusEnum type : values()) {
			map.put(type.getCode(), type.getMessage());
		}
		return map;
	}
	
	/**
	 * 通过枚举值码查找枚举值。
	 *
	 * @param code 查找枚举值的枚举值码。
	 * @return 枚举值码对应的枚举值。
	 * @throws IllegalArgumentException 如果 code 没有对应的 Status 。
	 */
	public static CmbStatusEnum find(String code) {
		for (CmbStatusEnum status : values()) {
			if (status.getCode().equals(code)) {
				return status;
			}
		}
		throw new IllegalArgumentException("BoscAuditStatusEnum not legal:" + code);
	}
	
	/**
	 * 获取全部枚举值。
	 *
	 * @return 全部枚举值。
	 */
	public static List<CmbStatusEnum> getAll() {
		List<CmbStatusEnum> list = new ArrayList<CmbStatusEnum> ();
		for (CmbStatusEnum status : values()) {
			list.add(status);
		}
		return list;
	}
	
	/**
	 * 获取全部枚举值码。
	 *
	 * @return 全部枚举值码。
	 */
	public static List<String> getAllCode() {
		List<String> list = new ArrayList<String>();
		for (CmbStatusEnum status : values()) {
			list.add(status.code());
		}
		return list;
	}
		
	}
