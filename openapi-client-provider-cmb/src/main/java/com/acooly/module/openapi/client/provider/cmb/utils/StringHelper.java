package com.acooly.module.openapi.client.provider.cmb.utils;

import com.acooly.core.utils.Encodes;

import java.util.Map;

public class StringHelper {

    /**
     * 生成不重复随机字符串包括字母数字
     *
     * @param len
     * @return
     */
    public static String generateRandomStr(int len) {
        //字符源，可以根据需要删减
        String generateSource = "0123456789abcdefghigklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String rtnStr = "";
        for (int i = 0; i < len; i++) {
            //循环随机获得当次字符，并移走选出的字符
            String nowStr = String.valueOf(generateSource.charAt((int) Math.floor(Math.random() * generateSource.length())));
            rtnStr += nowStr;
            generateSource = generateSource.replaceAll(nowStr, "");
        }
        return rtnStr;
    }

    /**
     * 将map中的值转化为字符，并以|分开
     *
     * @param params
     * @return
     */
    public static String mapToStr(Map<String, Object> params) {
        StringBuffer returnStr = new StringBuffer();
        for (Map.Entry<String, Object> entry : params.entrySet()) {
            returnStr.append(entry.getValue() + "|");
        }
        return returnStr.substring(0, returnStr.length() - 1);
    }

    /**
     * 将map中的key和value转化为字符，并以index分开
     *
     * @param params
     * @return
     */
    public static String mapToStrByIndex(Map<String, Object> params, String index) {
        StringBuffer returnStr = new StringBuffer();
        for (Map.Entry<String, Object> entry : params.entrySet()) {
            returnStr.append(entry.getKey() + "=" + entry.getValue() + index);
        }
        return returnStr.substring(0, returnStr.length() - index.length());
    }

    /**
     * 将byte[] 转换成字符串
     *
     * @return
     */
    public static String byte2Hex(byte[] srcBytes) {
        StringBuilder hexRetSB = new StringBuilder();
        for (byte b : srcBytes) {
            String hexString = Integer.toHexString(0x00ff & b);
            hexRetSB.append(hexString.length() == 1 ? 0 : "").append(hexString);
        }
        return hexRetSB.toString();
    }

    /**
     * 将16进制字符串转为转换成字符串
     *
     * @param source
     * @return
     */
    public static byte[] hex2Bytes(String source) {
        byte[] sourceBytes = new byte[source.length() / 2];
        for (int i = 0; i < sourceBytes.length; i++) {
            sourceBytes[i] = (byte) Integer.parseInt(source.substring(i * 2, i * 2 + 2), 16);
        }
        return sourceBytes;
    }

    /**
     * 组装跳转url
     *
     * @param redirectUrl
     * @param redirectParams
     * @return
     */
    public static String getRedirectUrl(String redirectUrl, Map<String, String> redirectParams) {
        StringBuffer stringBuffer = new StringBuffer();
        for (Map.Entry<String, String> entry : redirectParams.entrySet()) {
            stringBuffer.append(entry.getKey() + "=" + Encodes.urlEncode(entry.getValue()) + "&");
        }
        return redirectUrl + "?" + stringBuffer.substring(0, stringBuffer.length() - 1);
    }
}
