package com.acooly.openapi.client.provider.wewallet;

import com.acooly.core.common.BootApp;
import com.acooly.module.openapi.client.provider.wewallet.WeWalletApiService;
import com.acooly.module.openapi.client.provider.wewallet.message.WeWalletTicketRequest;
import com.acooly.module.openapi.client.provider.wewallet.message.WeWalletTicketResponse;
import com.acooly.module.openapi.client.provider.wewallet.message.WeWalletTokenRequest;
import com.acooly.module.openapi.client.provider.wewallet.message.WeWalletTokenResponse;
import com.acooly.test.NoWebTestBase;
import com.alibaba.fastjson.JSON;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@BootApp(sysName = "weWalletTest")
public class WeWalletTest extends NoWebTestBase {

    @Autowired
    private WeWalletApiService weWalletApiService;

    /**
     * 获取访问 令牌 接口
     */
    @Test
    public void testWeWalletToken() {

        WeWalletTokenRequest request = new WeWalletTokenRequest();

        request.setGatewayUrl("https://test-open.webank.com/api/oauth2/access_token");
        request.setAppId("WW000001");
        request.setSecret("WW000001");
        request.setGrantType("client_credential");
        request.setVersion("1.0.0");

        try {
            WeWalletTokenResponse response = weWalletApiService.weWalletToken(request);
            System.out.println("获取访问令牌接口测试执行结果：" + JSON.toJSONString(response));
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("异常原因：" + e.getMessage());
        }
    }


    /**
     * 获取API票据
     */
    @Test
    public void testWeWalletTicket() {

        WeWalletTicketRequest request = new WeWalletTicketRequest();
        request.setGatewayUrl("https://test-open.webank.com/api/oauth2/access_token");
        request.setAppId("WW000001");
        request.setAccessToken("accessToken_string");
        request.setType("SIGN");
        request.setUserId("123456");
        request.setVersion("1.0.0");

        try {
            WeWalletTicketResponse response = weWalletApiService.weWalletTicket(request);
            System.out.println("获取API票据接口测试执行结果：" + JSON.toJSONString(response));
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("异常原因：" + e.getMessage());
        }
    }

}

