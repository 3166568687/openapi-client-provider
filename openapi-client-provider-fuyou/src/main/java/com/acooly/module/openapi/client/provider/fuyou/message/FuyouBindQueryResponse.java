package com.acooly.module.openapi.client.provider.fuyou.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.fuyou.domain.FuyouApiMsgInfo;
import com.acooly.module.openapi.client.provider.fuyou.domain.FuyouResponse;
import com.acooly.module.openapi.client.provider.fuyou.enums.FuyouServiceEnum;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

/**
 * @author zhike 2018/3/19 15:51
 */
@Getter
@Setter
@FuyouApiMsgInfo(service = FuyouServiceEnum.FUYOU_BIND_QUERY,type = ApiMessageType.Response)
@XStreamAlias("RESPONSE")
public class FuyouBindQueryResponse extends FuyouResponse{

    /**
     * 用户编号
     * 商户端用户的唯一编号，即用户 ID
     */
    @XStreamAlias("USERID")
    @NotBlank
    @Size(max = 40)
    private String userId;

    /**
     * 协议号
     * 首次协议交易成功好生成的协议号
     */
    @XStreamAlias("PROTOCOLNO")
    @NotBlank
    @Size(max = 30)
    private String protocolNo;

    /**
     * 银行卡号
     */
    @XStreamAlias("CARDNO")
    @NotBlank
    @Size(max = 20)
    private String cardNo;
}
