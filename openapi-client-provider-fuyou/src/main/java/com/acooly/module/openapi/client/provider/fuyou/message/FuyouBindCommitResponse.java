package com.acooly.module.openapi.client.provider.fuyou.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.fuyou.domain.FuyouApiMsgInfo;
import com.acooly.module.openapi.client.provider.fuyou.domain.FuyouResponse;
import com.acooly.module.openapi.client.provider.fuyou.enums.FuyouServiceEnum;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

/**
 * @author zhike 2018/3/19 15:51
 */
@Getter
@Setter
@FuyouApiMsgInfo(service = FuyouServiceEnum.FUYOU_BIND_COMMIT,type = ApiMessageType.Response)
@XStreamAlias("RESPONSE")
public class FuyouBindCommitResponse extends FuyouResponse{

    /**
     * 商户流水号，保持唯一
     */
    @XStreamAlias("MCHNTSSN")
    @NotBlank
    @Size(max = 30)
    private String orderNo;

    /**
     * 协议号
     * 交易成功好默认绑定五要素的协议
     * 号（信用卡暂不支持绑定协议）
     */
    @XStreamAlias("PROTOCOLNO")
    @NotBlank
    @Size(max = 30)
    private String protocolNo;

    /**
     * 签名类型
     */
    @XStreamAlias("SIGNTP")
    private String signType;
}
