package com.acooly.module.openapi.client.provider.yuejb.message;

import com.acooly.module.openapi.client.provider.yuejb.domain.YueJBResponse;
import lombok.Data;

/**
 * Created by ouwen@yiji.com} on 2017/11/7.
 */
@Data
public class YueJBLoanResponse extends YueJBResponse {
    /**
     * 放款订单号
     */
    private String loanNo;
}
