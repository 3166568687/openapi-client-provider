package com.acooly.module.openapi.client.provider.newyl.exception;

/**
 * @author zhike 2018/2/6 13:21
 */
public class ApiClientYlProcessingException extends RuntimeException{

    private static final long serialVersionUID = 788548256305224365L;

    public ApiClientYlProcessingException() {
    }

    public ApiClientYlProcessingException(String message, Throwable cause) {
        super(message, cause);
    }

    public ApiClientYlProcessingException(String message) {
        super(message);
    }

    public ApiClientYlProcessingException(Throwable cause) {
        super(cause);
    }
}
