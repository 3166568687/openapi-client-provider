package com.acooly.module.openapi.client.provider.newyl.marshall;

import com.acooly.module.openapi.client.api.marshal.ApiMarshal;
import com.acooly.module.openapi.client.api.message.PostRedirect;
import com.acooly.module.openapi.client.provider.newyl.domain.NewYlRequest;

import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;


@Service
@Slf4j
public class NewYlRedirectPostMarshall extends NewYlMarshallSupport implements ApiMarshal<PostRedirect, NewYlRequest> {

    @Override
    public PostRedirect marshal(NewYlRequest source) {

        return null;
    }

}
