package com.acooly.module.openapi.client.provider.fbank.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.fbank.domain.FbankApiMsgInfo;
import com.acooly.module.openapi.client.provider.fbank.domain.FbankResponse;
import com.acooly.module.openapi.client.provider.fbank.enums.FbankServiceEnum;
import com.acooly.module.openapi.client.provider.fbank.enums.FbankServiceTypeEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

/**
 * @author zhike@qq.com
 * @date 2018-09-06 16:08
 */
@Getter
@Setter
@FbankApiMsgInfo(service = FbankServiceEnum.PREPAY_API, type = ApiMessageType.Response, serviceType = FbankServiceTypeEnum.payol)
public class FbankPrepayResponse extends FbankResponse {

    /**
     * 平台订单号
     * 支付平台生成的订单号（唯一）
     */
    @NotBlank
    @Size(max = 32)
    private String orderNo;

    /**
     * 商户订单号
     * 商户自己平台的订单号（唯一）
     */
    @NotBlank
    @Size(max = 64)
    private String mchntOrderNo;

    /**
     * 支付功能id
     */
    @NotBlank
    @Size(max = 12)
    private String payPowerId;

    /**
     * 二维码链接
     * 支付宝扫码、微信扫码时返回
     */
    @Size(max = 128)
    private String code_url;

    /**
     * 支付宝授权链接
     * 当payPowerId= 3或payPowerId =17时，商户下单时未传openId参数时返回;
     * 商户拿到此链接后，要放在支付宝内部页面进行打开，从而获取用户支付宝授权。
     */
    @Size(max = 128)
    private String authUrl;

    /**
     * 上游订单号
     * 条码支付成功时有返回(支付宝、微信)
     * <p>
     * 当payPowerId= 3或payPowerId =17时，商户下单时传openId参数时有返回;
     * 通过支付宝交易号唤起支付宝jsapi支付请参考：
     * https://docs.open.alipay.com/common/105591
     */
    @Size(max = 64)
    private String bankTransactionId;

    /**
     * 唤起支付的参数
     * JSON格式。
     * 唤起微信APP支付请参考：
     * https://pay.weixin.qq.com/wiki/doc/api/app/app.php?chapter=8_1
     * 唤起微信公众号支付请参考：
     * https://pay.weixin.qq.com/wiki/doc/api/jsapi_sl.php?chapter=7_7&index=6
     * 唤起qq公众号支付请参考：
     * https://qpay.qq.com/qpaywiki/showdocument.php?pid=99&docid=182
     * 唤起微信小程序支付请参考：
     * https://pay.weixin.qq.com/wiki/doc/api/wxa/wxa_sl_api.php?chapter=7_7&index=5
     */
    @Size(max = 256)
    private String prepay_info;

    /**
     * 微信h5接口支付链接
     * 微信h5接口时有返回；微信h5原生支付链接，需要通过页面来发起请求，获取后不要直接复制到手机浏览器进行测试。
     * 请求失败请参考：
     * https://pay.weixin.qq.com/wiki/doc/api/H5_sl.php?chapter=15_4
     * 如果需要支付完成后进行同步跳转，请参考下列链接中，常见问题-回调页面栏目：
     * https://pay.weixin.qq.com/wiki/doc/api/H5_sl.php?chapter=15_4
     * <p>
     * 京东H5支付时，如果请求参数中jdH5ReturnType为mweb_url，则此参数为唤起支付的url。
     */
    @Size(max = 256)
    private String mweb_url;

    /**
     * 条码支付成功时有返回(支付宝、微信)
     * 订单创建时间；格式：yyyyMMdd
     */
    @Size(max = 8)
    private String orderDt;

    /**
     * 订单手续费
     * 条码支付成功时有返回(支付宝、微信)
     * 该笔订单所扣费用（单位为分）
     */
    @NotBlank
    @Size(max = 12)
    private String fee;

    /**
     * 支付完成时间
     * 条码支付成功时有返回(支付宝、微信)
     * 格式：yyyy-MM-dd HH:mm:ss
     */
    @Size(max = 19)
    private String paidTime;

    /**
     * 支付用户信息
     * 条码支付成功时有返回(支付宝、微信)
     * 该笔订单的支付用户信息
     */
    @NotBlank
    @Size(max = 64)
    private String openId;

    /**
     * 商户订单描述
     * 条码支付成功时有返回(支付宝、微信)
     */
    @Size(max = 64)
    private String body;

    /**
     * 商户订单标题
     * 条码支付成功时有返回(支付宝、微信)
     */
    @Size(max = 256)
    private String subject;

    /**
     * 支付结果
     * 条码支付成功时有返回(支付宝、微信)
     * 0:待支付;1:支付中(已请求上游);2:支付成功;3:支付失败;4:退款;5:已关闭;6:撤销;
     */
    @NotBlank
    @Size(max = 1)
    private String paySt;

    /**
     * 京东pc网页、京东h5网页支付返回页面html（京东支付会返回该字段）
     * 京东pc网页、京东h5网页支付有返回，返回内容为html,商户需要响应用户端。
     */
    private String jdResult;
}
