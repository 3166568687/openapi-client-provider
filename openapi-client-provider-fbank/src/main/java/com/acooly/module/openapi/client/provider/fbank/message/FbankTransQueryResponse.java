package com.acooly.module.openapi.client.provider.fbank.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.fbank.domain.FbankApiMsgInfo;
import com.acooly.module.openapi.client.provider.fbank.domain.FbankResponse;
import com.acooly.module.openapi.client.provider.fbank.enums.FbankServiceEnum;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

/**
 * @author zhike@qq.com
 * @date 2018-09-06 11:48
 */
@Slf4j
@Getter
@Setter
@FbankApiMsgInfo(service = FbankServiceEnum.TRADE_QUERY, type = ApiMessageType.Response)
public class FbankTransQueryResponse extends FbankResponse {

    /**
     * 订单总金额
     * 下单请求时的金额（单位为分）
     */
    @NotBlank
    @Size(max = 12)
    private String amount;

    /**
     * 商品名称
     * 商户下单时所传
     */
    @NotBlank
    @Size(max = 64)
    private String subject;

    /**
     * 订单描述
     * 商户下单时所传
     */
    @Size(max = 255)
    private String body;

    /**
     * 商户订单号
     * 商户自己平台的订单号（唯一）
     */
    @Size(max = 64)
    private String mchntOrderNo;

    /**
     * 支付功能id
     */
    @Size(max = 12)
    private String payPowerId;

    /**
     * 支付平台订单号
     * 支付平台生成的订单号（唯一）
     */
    @Size(max = 32)
    private String orderNo;

    /**
     * 支付结果
     * 0:待支付;1:支付中(已请求上游);2:支付成功;3:支付失败;4:退款;5:已关闭;6:撤销;
     */
    @Size(max = 1)
    private String paySt;

    /**
     * 第三方流水号
     * 第三方流水号(支付宝、微信)
     */
    @Size(max = 64)
    private String bankTransactionId;

    /**
     * 下单日期
     * 订单创建时间；格式：yyyyMMdd
     */
    @Size(max = 8)
    private String orderDt;

    /**
     * 支付完成时间
     * 格式：yyyy-MM-dd HH:mm:ss
     */
    @Size(max = 30)
    private String timePaid;

    /**
     * 订单附加描述
     * 商户下单时所传
     */
    @Size(max = 512)
    private String description;

    /**
     * 订单手续费
     * 该笔订单所扣费用（单位为分）
     */
    @Size(max = 12)
    @NotBlank
    private String fee;

    /**
     * 平台订单创建时间
     * 商户下单的时间，格式：yyyy-MM-dd HH:mm:ss
     */
    @Size(max = 30)
    private String created;

    /**
     * 支付渠道id
     * 支付渠道id，具体查看数据字典
     */
    @Size(max = 2)
    @NotBlank
    private String payChannelId;

    /**
     * 商户异步通知地址
     * 富民通知商户的地址
     */
    @Size(max = 128)
    private String notifyUrl;

}
