package com.acooly.module.openapi.client.provider.cj;

import com.acooly.module.openapi.client.api.transport.HttpTransport;
import com.acooly.module.openapi.client.api.transport.Transport;

import org.apache.http.entity.ContentType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import static com.acooly.module.openapi.client.provider.cj.CjProperties.PREFIX;


@EnableConfigurationProperties({CjProperties.class})
@ConditionalOnProperty(value = PREFIX + ".enable", matchIfMissing = true)
@ComponentScan
public class CjConfigration {

    @Autowired
    private CjProperties cjProperties;

    @Bean("cjHttpTransport")
    public Transport weBankHttpTransport() {
        HttpTransport httpTransport = new HttpTransport();
        httpTransport.setGateway(cjProperties.getGatewayUrl());
        httpTransport.setConnTimeout(String.valueOf(cjProperties.getConnTimeout()));
        httpTransport.setReadTimeout(String.valueOf(cjProperties.getReadTimeout()));
        httpTransport.setContentType(ContentType.TEXT_XML.getMimeType());
        return httpTransport;
    }
}
