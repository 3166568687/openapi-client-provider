package com.acooly.module.openapi.client.provider.cj.message.dto;

import com.acooly.core.utils.Money;
import com.acooly.core.utils.validate.jsr303.MoneyConstraint;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

/**
 * 签约订单参数
 * @author fufeng
 */
@Getter
@Setter
public class CjBatchRetInfo implements Serializable {

    /**
     * 订单号
     */
    private String bizOrderNo;


    private String status;

    /**
     * 金额 (整数，单位分)
     */
    @MoneyConstraint(min = 1L)
    private Money amount;

    /**
     * 接口状态码
     * 成功、失败返回
     */
    private String retCode;

    /**
     * 接口状态描述
     * 成功、失败返回
     */
    private String errMsg;

}
