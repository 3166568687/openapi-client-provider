<!-- title: 银盛支付SDK组件 -->
<!-- type: gateway -->
<!-- author: zhike -->

## 1. 提供能力

组件的集成模式为标准组件集成方式。直接通过pom文件引入依赖并配置对应的参数即可。

## 2. 使用说明

```xml
<dependency>
  <groupId>com.acooly</groupId>
  <artifactId>openapi-client-provider-yinsheng</artifactId>
  <version>4.2.0-SNAPSHOT</version>
</dependency>
```

直接采用yinsheng的SDK，然后扩展异步通知处理
使用方式：
1、在自己工程注入YinShengApiService

2、此sdk提供银盛的网关直连支付、无卡快捷（不需要银盛发送异步通知）、订单查询、对账下载

3、密钥加载支持两种方式
   a：通过配置文件配置密钥加载路径；
   b: 在自己工程实现接口YinShengPartnerIdLoadManager，通过传入一个标识性字符串如（partnerId）加载公钥、私钥路径以及私钥密码
   c: 因为银盛接口比较坑异步通知的时候除了订单号没有返回接口任何其它唯一表示字段，如果是托管模式，无法知道此笔异步通知的密钥，所以此sdk提供了
      接口YinShengPartnerIdLoadManager通过传入订单号，获取此笔订单对应银盛的partnerId，在加上provider标识，获取当前的密钥对象，用于
      异步通知验签

4、本sdk中网关支付和快捷支付的异步通知地址固定值，所以调用的时候不需要传入notify_url：请参考com.acooly.module.openapi.client.provider.yinsheng.OpenAPIClientYinShengConfigration.getApiSDKServlet
   a:网关支付：/gateway/notify/ysNotify/yinShengNetBank
   b:快捷支付：/gateway/notify/ysNotify/yinShengFastPay
   接收异步通知的时候实现接口NotifyHandler，重新handleNotify和serviceKey方法，其中derviceKey方法需要绑定的key值在枚举YinShengServiceEnum可以获取到
   快捷支付异步通知接收示例：
   @Slf4j
   @Service
   public class YsFastPayNotifyService implements NotifyHandler{

       @Autowired
       private GatewayDeductTransactionService gatewayDeductTransactionService;

       @Override
       public void handleNotify(ApiMessage notify) {
           YinShengFastPayNotify fastPayNotify = (YinShengFastPayNotify)notify;
           log.info("银盛快捷异步通知报文：{}", JSON.toJSONString(fastPayNotify));
           String orderNo = fastPayNotify.getOut_trade_no();
           GatewayDeductTransaction deductTransaction = gatewayDeductTransactionService.findEntityBysettleSerialNo(orderNo);
           MDC.put("gid", deductTransaction.getGid());
           log.info("银盛快捷支付订单orderNo={}异步处理开始", orderNo);
           if (deductTransaction == null) {
               log.info("此笔流水bizOrderNo={}不存在", orderNo);
               return;
           } else if (deductTransaction.getStatus() == MusGwStatusEnum.BS || deductTransaction.getStatus() == MusGwStatusEnum.BF) {
               log.info("此笔流水bizOrderNo={}已处理", orderNo);
               return;
           }
           deductTransaction.setSettleDate(Dates.parse(fastPayNotify.getAccount_date(), "yyyyMMdd"));
           deductTransaction.setSucAmount(Money.amout(fastPayNotify.getTotal_amount()).getCent());
           deductTransaction.setNotifyMessage(JSON.toJSONString(fastPayNotify));
           if (Strings.equals(fastPayNotify.getTrade_status(), "TRADE_SUCCESS")||Strings.equals(fastPayNotify.getTrade_status(), "TRADE_FINISHED")) {
               deductTransaction.setStatus(MusGwStatusEnum.BS);
           } else if(Strings.equals(fastPayNotify.getTrade_status(), "TRADE_CLOSED")||
                   Strings.equals(fastPayNotify.getTrade_status(), "TRADE_SUCCESS")||
                   Strings.equals(fastPayNotify.getTrade_status(), "TRADE_SUCCESS")) {
               deductTransaction.setStatus(MusGwStatusEnum.BF);
           } else {
               log.info("银盛快捷交易状态trade_status={}不明，不处理",fastPayNotify.getTrade_status());
           }
           gatewayDeductTransactionService.update(deductTransaction);

       }

       @Override
       public String serviceKey() {
           return YinShengServiceEnum.FAST_PAY.getKey();
       }
   }

5、其中网关支付跳转通知，和异步通知参数一致所以复用了异步通知实体，需要自己创建一个controller来接收，请求的时候手动传入retrunUrl，然后通过方法com.acooly.module.openapi.client.provider.yinsheng.YinShengApiService.notice
   将接收到的参数转化为异步通知实体
   网关支付跳转通知接收示例：
    /**
     * 银盛网关跳回商户页面
     */
    @RequestMapping("/ysNetBankRedirect")
    public void ysNetBankSendRedirect(HttpServletRequest request, HttpServletResponse response) {
        try {
            YinShengEbankDepositNotify yinShengEbankDepositNotify = (YinShengEbankDepositNotify) yinShengApiService.notice(request, YinShengServiceEnum.NETBANK_DEPOSIT.getKey());
            //订单号
            String orderNo = yinShengEbankDepositNotify.getOut_trade_no();
            //订单状态
            String status = yinShengEbankDepositNotify.getTrade_status();
            NetbankPaymentTxn netbankPaymentTxn = netbankPaymentTxnService.findBySendBankSerial(orderNo);

            if (netbankPaymentTxn == null) {
                log.info("此笔流水sendBankSerial={}不存在", orderNo);
                return;
            }

            if (Strings.isBlank(netbankPaymentTxn.getNotifyFrontUrl())) {
                log.info("此笔流水sendBankSerial={}跳转url不存在，不跳回商户页面", orderNo);
                return;
            }

            String amount = yinShengEbankDepositNotify.getTotal_amount();
            //组装跳转returnUrl
            ApiNotifyOrder returnOrder = new ApiNotifyOrder();
            returnOrder.setPartnerId(netbankPaymentTxn.getPartnerId());
            returnOrder.setGid(netbankPaymentTxn.getGid());
            returnOrder.setParameter("amount", amount);
            returnOrder.setParameter("payerMerchUserId", netbankPaymentTxn.getUserId());
            returnOrder.setParameter("profitAmount", "0");
            returnOrder.setParameter("shareProfitAmount", "0");
            returnOrder.setParameter("tradeTime", Dates.format(new Date()));
            returnOrder.setParameter("tradeType", "NET_DEPOSIT");
            if (Strings.equals(status, "TRADE_SUCCESS")) {
                returnOrder.setParameter("tradeMemo", "交易成功");
                returnOrder.setParameter("tradeStatus", "SUCCESS");
            } else {
                returnOrder.setParameter("tradeMemo", "交易失败");
                returnOrder.setParameter("tradeStatus", "FAIL");
            }
            ApiNotifyResult retrunNotifyResult = openApiRemoteService.syncReturn(returnOrder);
            String sendRedirectUrl = retrunNotifyResult.getCompleteReturnUrl();
            log.info("银盛跳转地址:{}", retrunNotifyResult.getCompleteReturnUrl());
            response.sendRedirect(sendRedirectUrl);
            log.info("银盛网关支付订单orderNo={}跳转处理成功:{}", orderNo);
        } catch (Exception e) {
            log.info("银盛网关支付跳转处理失败:{}", e.getMessage());
        }
    }


6、接口的服务码和对应的唯一表示统一放在com.acooly.module.openapi.client.provider.yinsheng.enums.YinShengServiceEnum

注意：调用接口的时候method不用手动传入，如果是配置文件方式partnerId也不用传，sign_type默认为RSA,version默认为3.0，charset默认为UTF-8
     timestamp默认为当前时间,配置文件方式不用传入请求地址

7、sdk提供了单独签名、验签、异步实体转化方法

8、一些接口需要传入business_code和seller_name,这两个参数会放到配置文件中，使用的时候请配置好，注入OpenAPIClientYinShengProperties后获取，
    也可以在自己的业务系统管理，然后传入

9、配置文件示例：
acooly.openapi.client.yinsheng.enable=true
acooly.openapi.client.yinsheng.gatewayUrl=https://mertest.ysepay.com/openapi_gateway/gateway.do
acooly.openapi.client.yinsheng.publicKeyPath=classpath:keystore/test/ys/businessgate.cer
acooly.openapi.client.yinsheng.privateKeyPath=classpath:keystore/test/ys/shanghu_test.pfx
acooly.openapi.client.yinsheng.privateKeyPassword=123456
acooly.openapi.client.yinsheng.domain=http://218.70.106.250:8881
acooly.openapi.client.yinsheng.connTimeout=10000
acooly.openapi.client.yinsheng.readTimeout=30000
acooly.openapi.client.yinsheng.partnerId=shanghu_test
acooly.openapi.client.yinsheng.notifyUrlPrefix=/
acooly.openapi.client.yinsheng.businessCode=01000010
acooly.openapi.client.yinsheng.sellerName=银盛支付商户测试公司
