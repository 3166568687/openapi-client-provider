/*
 * ouwen@yiji.com Inc.
 * Copyright (c) 2017 All Rights Reserved.
 * create by ouwen
 * date:2017-03-30
 *
 */
package com.acooly.module.openapi.client.provider.yinsheng.enums;

import com.acooly.core.utils.enums.Messageable;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * inst_channel_api DebitCreditType 枚举定义
 *
 * @author zhike
 * Date: 2017-03-30 15:12:06
 */
public enum YinShengEbankDepositSupportBankEnum implements Messageable {

    ICBC("1021000", "工商银行"),
    ABC("1031000", "农业银行"),
    BOC("1041000", "中国银行"),
    CCB("1051000", "建设银行"),
    CMBC("3051000", "民生银行"),
    CNCB("3021000", "中信银行"),
    PAB("3071000", "平安银行"),
    SPDB("3102900", "浦发银行"),
    CEB("3031000", "光大银行"),
    BCCB("3131000", "北京银行"),
    PSBC("4031000", "中国邮政储蓄银行"),
    BOS("3132900", "上海银行"),

    ;
    private final String code;
    private final String message;

    private YinShengEbankDepositSupportBankEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String code() {
        return code;
    }

    public String message() {
        return message;
    }

    public static Map<String, String> mapping() {
        Map<String, String> map = new LinkedHashMap<String, String>();
        for (YinShengEbankDepositSupportBankEnum type : values()) {
            map.put(type.getCode(), type.getMessage());
        }
        return map;
    }

    /**
     * 通过枚举值码查找枚举值。
     *
     * @param code 查找枚举值的枚举值码。
     * @return 枚举值码对应的枚举值。
     * @throws IllegalArgumentException 如果 code 没有对应的 Status 。
     */
    public static YinShengEbankDepositSupportBankEnum find(String code) {
        for (YinShengEbankDepositSupportBankEnum status : values()) {
            if (status.getCode().equals(code)) {
                return status;
            }
        }
        return null;
    }

    /**
     * 获取全部枚举值。
     *
     * @return 全部枚举值。
     */
    public static List<YinShengEbankDepositSupportBankEnum> getAll() {
        List<YinShengEbankDepositSupportBankEnum> list = new ArrayList<YinShengEbankDepositSupportBankEnum>();
        for (YinShengEbankDepositSupportBankEnum status : values()) {
            list.add(status);
        }
        return list;
    }

    /**
     * 获取全部枚举值码。
     *
     * @return 全部枚举值码。
     */
    public static List<String> getAllCode() {
        List<String> list = new ArrayList<String>();
        for (YinShengEbankDepositSupportBankEnum status : values()) {
            list.add(status.code());
        }
        return list;
    }

}
