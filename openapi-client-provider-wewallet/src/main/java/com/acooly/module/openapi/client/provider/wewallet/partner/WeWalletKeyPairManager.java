/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-11-15 13:21 创建
 */
package com.acooly.module.openapi.client.provider.wewallet.partner;

import com.acooly.module.safety.support.KeyPair;

/**
 * 标记KeyStoreInfo的load 标记接口
 *
 * @author zhangpu 2017-11-15 13:21
 */
public interface WeWalletKeyPairManager {

    /**
     * 根据商户号得到公私钥
     * @param partnerId
     * @return
     */
    KeyPair getKeyPair(String partnerId);
}
