/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhike
 * date:2016年4月29日
 *
 */
package com.acooly.module.openapi.client.provider.sinapay.message.member;

import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayApiMsg;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayRequest;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayAccountType;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Size;

/**
 * @author zhike
 */
@Getter
@Setter
@SinapayApiMsg(service = SinapayServiceNameEnum.QUERY_ACCOUNT_DETAILS, type = ApiMessageType.Request)
public class QueryAccountDetailsRequest extends SinapayRequest {

	/** 用户标识信息 */
	@Size(max = 50)
	@NotEmpty
	@ApiItem("identity_id")
	private String identityId;

	/** 用户标识类型 */
	@Size(max = 16)
	@NotEmpty
	@ApiItem("identity_type")
	private String identityType = "UID";

	/** 认证类型 */
	@Size(max = 20)
	@NotEmpty
	@ApiItem("account_type")
	private String accountType = SinapayAccountType.BASIC.code();

	/**
	 * 开始时间 数字串，一共14位 格式为：年[4位]月[2位]日[2位]时[2位]分[2位]秒[2位]
	 */
	@Size(max = 14)
	@DateTimeFormat(pattern = "yyyyMMddHHmmss")
	@NotEmpty
	@ApiItem("start_time")
	private String startTime;

	/**
	 * 结束时间 数字串，一共14位 格式为：年[4位]月[2位]日[2位]时[2位]分[2位]秒[2位]
	 */
	@Size(max = 14)
	@DateTimeFormat(pattern = "yyyyMMddHHmmss")
	@NotEmpty
	@ApiItem("end_time")
	private String endTime;

	/** 页号，从1开始，默认为1 */
	@ApiItem("page_no")
	private int pageNo;

	/** 每页记录数，不超过30，默认20 */
	@ApiItem("page_size")
	private int pageSize;
}
