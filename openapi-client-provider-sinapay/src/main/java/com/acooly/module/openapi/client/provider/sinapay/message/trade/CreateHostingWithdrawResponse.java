package com.acooly.module.openapi.client.provider.sinapay.message.trade;

import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.sinapay.annotation.SinaGateWay;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayApiMsg;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayResponse;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinaWithdrawStatus;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayGateWayType;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * 3.12 提现
 *
 * @author xiaohong
 * @create 2018-07-18 11:04
 **/
@Getter
@Setter
@SinaGateWay(SinapayGateWayType.PAYMENT)
@SinapayApiMsg(service = SinapayServiceNameEnum.CREATE_HOSTING_WITHDRAW, type = ApiMessageType.Response)
public class CreateHostingWithdrawResponse extends SinapayResponse {
    /**
     * 提现订单号
     *
     * 商户网站交易订单号，商户内部保证唯一
     */
    @NotEmpty
    @Size(max = 32)
    @ApiItem(value = "out_trade_no")
    private String outTradeNo;

    /**
     * 提现状态
     *
     * 支付状态，详见附录“提现状态”
     */
    @Size(max = 16)
    @ApiItem(value = "withdraw_status")
    private String withdrawStatus = SinaWithdrawStatus.INIT.getCode();

    /**
     * 收银台重定向地址
     *
     * 当请求参数中的“version”的值是“1.1”时，且提现方式选择“CASHDESK”，此参数不为空。
     * 商户系统需要将用户按此参数的值重定向到新浪收银台。其他情况不返回此值，“version”的值是“1.0”时也不返回此值。
     */
    @Size(max = 200)
    @ApiItem(value = "redirect_url")
    private String redirectUrl;
}
