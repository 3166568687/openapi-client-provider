package com.acooly.module.openapi.client.provider.sinapay.message.member;

import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayApiMsg;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayRequest;
import com.acooly.module.openapi.client.provider.sinapay.enums.IsMaskEnum;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinaMemberTypeEnum;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * @author zhike 2018/7/10 15:03
 */
@Getter
@Setter
@SinapayApiMsg(service = SinapayServiceNameEnum.QUERY_MEMBER_INFOS, type = ApiMessageType.Request)
public class QueryMemberInfosRequest extends SinapayRequest {
    /**
     *用户标识信息
     * 商户系统用户ID(字母或数字)
     */
    @NotEmpty
    @Size(max = 50)
    @ApiItem(value = "identity_id")
    private String identityId;

    /**
     *用户标识类型
     * ID的类型，目前只包括UID
     */
    @NotEmpty
    @Size(max = 16)
    @ApiItem(value = "identity_type")
    private String identityType = "UID";

    /**
     *会员类型
     * 会员类型，详见附录，默认企业，且只支持企业
     * com.acooly.module.openapi.client.provider.sinapay.enums.SinaMemberTypeEnum
     */
    @Size(max = 16)
    @ApiItem(value = "member_type")
    private String memberType = SinaMemberTypeEnum.CORPORATION.getCode();

    /**
     * 是否掩码返回敏感信息
     * 掩码=Y，加密=N，默认为Y。敏感信息字段列表见说明
     * com.acooly.module.openapi.client.provider.sinapay.enums.IsMaskEnum
     */
    @Size(max = 16)
    @ApiItem(value = "is_mask")
    private String isMask = IsMaskEnum.Y.getCode();
}
