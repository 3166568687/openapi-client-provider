package com.acooly.module.openapi.client.provider.sinapay.message.trade;

import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.sinapay.annotation.SinaGateWay;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayApiMsg;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayRequest;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinaflagType;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayGateWayType;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

/**
 * 退款查询
 *
 * @author xiaohong
 * @create 2018-07-17 17:32
 **/
@Getter
@Setter
@SinaGateWay(SinapayGateWayType.PAYMENT)
@SinapayApiMsg(service = SinapayServiceNameEnum.QUERY_HOSTING_REFUND, type = ApiMessageType.Request)
public class QueryHostingRefundRequest extends SinapayRequest {
    /**
     * 用户标识信息
     */
    @NotEmpty
    @ApiItem(value = "identity_id")
    @Size(max = 50)
    private String identityId;

    /**
     * 用户标识类型
     *
     * ID的类型，参考“标志类型”
     */
    @NotEmpty
    @ApiItem(value = "identity_type")
    @Size(max = 16)
    private String identityType = SinaflagType.UID.getCode();

    /**
     * 退款订单号
     *
     * 商户网站交易订单号，商户内部保证唯一
     */
    @ApiItem(value = "out_trade_no")
    @Size(max = 32)
    private String outTradeNo;

    /**
     * 开始时间
     *
     * 数字串，一共14位 格式为：年[4位]月[2位]日[2位]时[2位]分[2位]秒[2位]
     */
    @ApiItem(value = "start_time")
    @Size(max = 14)
    private String startTime;


    /**
     * 结束时间
     *
     * 数字串，一共14位 格式为：年[4位]月[2位]日[2位]时[2位]分[2位]秒[2位]
     */
    @ApiItem(value = "end_time")
    @Size(max = 14)
    private String endTime;

    /**
     * 页号
     */
    @Min(1)
    @ApiItem(value = "page_no")
    private int pageNo = 1;

    /**
     * 每页大小
     */
    @Max(20)
    @ApiItem(value = "page_size")
    private int pageSize = 20;
}
