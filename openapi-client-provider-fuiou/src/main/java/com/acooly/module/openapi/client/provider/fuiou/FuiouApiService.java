/**
 * create by zhangpu
 * date:2015年3月20日
 */
package com.acooly.module.openapi.client.provider.fuiou;

import com.acooly.module.openapi.client.api.exception.ApiServerException;
import com.acooly.module.openapi.client.provider.fuiou.domain.FuiouResponse;
import com.acooly.module.openapi.client.provider.fuiou.enums.FuiouServiceEnum;
import com.acooly.module.openapi.client.provider.fuiou.message.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @author zhangpu
 */
@Service
public class FuiouApiService {

    public static final String SUCCESS = "0000";

    @Resource(name = "fuiouApiServiceClient")
    private FuiouApiServiceClient apiServiceClient;

    @Autowired
    private OpenAPIClientFuiouProperties openAPIClientFuiouProperties;


    /**
     * 开户注册
     * <p>
     * 无需激活，在用户首次充值时由第三方支付提示其激活
     *
     * @param request
     * @return
     */
    public FuiouResponse reg(FuiouRegRequest request) {
        request.setService("reg");
        return apiServiceClient.execute(request);
    }

    /**
     * 企业法人开户注册
     *
     * @param request
     * @return
     */
    public FuiouResponse artifReg(FuiouArtifRegRequest request) {
        request.setService("artifReg");
        return assertSuccess(apiServiceClient.execute(request));
    }

    /**
     * 查询余额
     *
     * @param request
     * @return
     */
    public BalanceActionResponse balanceAction(BalanceActionRequest request) {
        request.setService(FuiouServiceEnum.Balance.getKey());
        return (BalanceActionResponse) assertSuccess(apiServiceClient.execute(request));
    }

    /***
     * 网银充值请求（生成充值跳转URL）
     *
     * @param request
     * @return
     */
    public String depositRedirect(FuiouEbankDepositRequest request) {
        request.setNotifyUrl(openAPIClientFuiouProperties.getNotifyUrl(request.getService()));
        request.setReturnUrl(openAPIClientFuiouProperties.getDepositReturnUrl());
        return apiServiceClient.redirectGet(request);
    }

    /**
     * 网银充值同步返回报文解析
     *
     * @param data
     * @return
     */
    public FuiouEbankDepositResponse depositReturn(Map<String, String> data) {
        return (FuiouEbankDepositResponse) apiServiceClient.result(data, FuiouServiceEnum.Deposit.getKey());
    }

    /***
     * APP快速充值请求（生成充值跳转URL）
     *
     * @param request
     * @return
     */
    public String appQuickDepositRedirect(AppQuickDepositRequest request) {
        request.setNotifyUrl(openAPIClientFuiouProperties.getNotifyUrl(request.getService()));
        request.setReturnUrl(openAPIClientFuiouProperties.getAppDepositReturnUrl());
        return apiServiceClient.redirectGet(request);
    }

    /**
     * APP快速充值同步返回报文解析
     *
     * @param data
     * @return
     */
    public AppQuickDepositResponse appQuickDepositReturn(Map<String, String> data) {
        return (AppQuickDepositResponse) apiServiceClient.result(data, FuiouServiceEnum.AppQuickDeposit.getKey());
    }

    /***
     * APP快速提现请求（生成充值跳转URL）
     *
     * @param request
     * @return
     */
    public String appQuickWithdrawRedirect(AppQuickWithdrawRequest request) {
        request.setNotifyUrl(openAPIClientFuiouProperties.getNotifyUrl(request.getService()));
        request.setReturnUrl(openAPIClientFuiouProperties.getAppWithdrawRerurnUrl());
        return apiServiceClient.redirectGet(request);
    }

    /**
     * APP快速提现同步返回报文解析
     *
     * @param data
     * @return
     */
    public AppQuickWithdrawResponse appQuickWithdrawReturn(Map<String, String> data) {
        return (AppQuickWithdrawResponse) apiServiceClient.result(data, FuiouServiceEnum.AppQuickWithdraw.getKey());
    }

    /***
     * 提现请求（生成充值跳转URL）
     *
     * @param request
     * @return
     */
    public String withdrawRedirect(FuiouWithdrawRequest request) {
        request.setNotifyUrl(openAPIClientFuiouProperties.getNotifyUrl(request.getService()));
        request.setReturnUrl(openAPIClientFuiouProperties.getWithdrawRerurnUrl());
        return apiServiceClient.redirectGet(request);
    }

    /***
     * 提现同步返回报文解析
     *
     * @return
     */
    public FuiouWithdrawResponse withdrawReturn(Map<String, String> data) {
        return (FuiouWithdrawResponse) apiServiceClient.result(data, FuiouServiceEnum.Withdraw.getKey());
    }

    /**
     * 冻结
     *
     * @param request
     * @return
     */
    public FreezeResponse freeze(FreezeRequest request) {
        request.setService(FuiouServiceEnum.Freeze.getKey());
        return (FreezeResponse) assertSuccess(apiServiceClient.execute(request));
    }

    /**
     * 冻结转冻结
     *
     * @param request
     * @return
     */
    public TransferBuAndFreeze2FreezeResponse transferBuAndFreeze2Freeze(TransferBuAndFreeze2FreezeRequest request) {
        request.setService(FuiouServiceEnum.TransferBuAndFreeze2Freeze.getKey());
        return (TransferBuAndFreeze2FreezeResponse) assertSuccess(apiServiceClient.execute(request));
    }

    /**
     * 解冻
     *
     * @param request
     * @return
     */
    public UnFreezeResponse unFreeze(UnFreezeRequest request) {
        request.setService(FuiouServiceEnum.UnFreeze.getKey());
        return (UnFreezeResponse) assertSuccess(apiServiceClient.execute(request));
    }

    /**
     * 划拨冻结
     *
     * @param request
     * @return
     */
    public TransferBuAndFreezeResponse transferBuAndFreeze(TransferBuAndFreezeRequest request) {
        request.setService(FuiouServiceEnum.TransferBuAndFreeze.getKey());
        return (TransferBuAndFreezeResponse) assertSuccess(apiServiceClient.execute(request));
    }

    /**
     * 划拨 (个人与个人之间)
     *
     * @param request
     * @return
     */
    public TransferBuResponse transferBu(TransferBuRequest request) {
        request.setService(FuiouServiceEnum.TransferBu.getKey());
        return (TransferBuResponse) assertSuccess(apiServiceClient.execute(request));
    }

    /**
     * 转账 (商户与个人之间)
     *
     * @param request
     * @return
     */
    public TransferBmuResponse transferBmu(TransferBmuRequest request) {
        request.setService(FuiouServiceEnum.TransferBmu.getKey());
        return (TransferBmuResponse) assertSuccess(apiServiceClient.execute(request));
    }

    protected FuiouResponse assertSuccess(FuiouResponse response) {
        if (!SUCCESS.equals(response.getRespCode())) {
            throw new ApiServerException(response.getRespCode(), response.getMessage());
        }
        return response;
    }
}
