/**
 * create by zhangpu
 * date:2015年3月12日
 */
package com.acooly.module.openapi.client.provider.fuiou.marshall;

import com.acooly.core.utils.Reflections;
import com.acooly.core.utils.Strings;
import com.acooly.module.openapi.client.api.exception.ApiClientException;
import com.acooly.module.openapi.client.api.marshal.ApiUnmarshal;
import com.acooly.module.openapi.client.api.message.MessageFactory;
import com.acooly.module.openapi.client.provider.fuiou.FuiouConstants;
import com.acooly.module.openapi.client.provider.fuiou.domain.FuiouNotify;
import com.acooly.module.openapi.client.provider.fuiou.support.FuiouAlias;
import com.acooly.module.openapi.client.provider.fuiou.support.FuiouRespCodes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.lang.reflect.Field;
import java.util.Map;
import java.util.Set;

/**
 * @author zhangpu
 */
@Service
public class FuiouNotifyUnmarshall extends FuiouMarshallSupport
        implements ApiUnmarshal<FuiouNotify, Map<String, String>> {
    private static final Logger logger = LoggerFactory.getLogger(FuiouNotifyUnmarshall.class);
    @Resource(name="fuiouMessageFactory")
    private MessageFactory messageFactory;

    @SuppressWarnings("unchecked")
    @Override
    public FuiouNotify unmarshal(Map<String, String> message, String serviceName) {
        try {
            logger.info("异步通知{}", message);
            String signature = message.get("signature");
            String plain = getWaitForSign(message);
            getSignerFactory().getSigner(FuiouConstants.SIGNER_KEY).verify(signature, getKeyPair(), plain);
            return doUnmarshall(message, serviceName);
        } catch (Exception e) {
            throw new ApiClientException("异步通知 解析失败:" + e.getMessage());
        }

    }

    protected FuiouNotify doUnmarshall(Map<String, String> message, String serviceName) {
        FuiouNotify notify = (FuiouNotify) messageFactory.getNotify(serviceName);
        Set<Field> fields = Reflections.getFields(notify.getClass());
        String key = null;
        for (Field field : fields) {
            FuiouAlias fuiouAlias = field.getAnnotation(FuiouAlias.class);
            if (fuiouAlias == null) {
                continue;
            }
            key = fuiouAlias.value();
            if (Strings.isBlank(key)) {
                key = field.getName();
            }
            Reflections.invokeSetter(notify, field.getName(), message.get(key));
        }
        notify.setService(serviceName);
        if (Strings.isNotBlank(notify.getRespCode())) {
            notify.setMessage(FuiouRespCodes.getMessage(notify.getRespCode()));
        }
        return notify;
    }

    public void setMessageFactory(MessageFactory messageFactory) {
        this.messageFactory = messageFactory;
    }

}
