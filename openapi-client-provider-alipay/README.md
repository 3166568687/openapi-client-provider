<!-- title: 支付宝支付SDK组件 -->
<!-- type: gateway -->
<!-- author: jiandao -->

## 1. 提供能力

组件的集成模式为标准组件集成方式。直接通过pom文件引入依赖并配置对应的参数即可。

## 2. 使用说明

```xml
<dependency>
  <groupId>com.acooly</groupId>
  <artifactId>openapi-client-provider-alipay</artifactId>
  <version>4.2.0-SNAPSHOT</version>
</dependency>
```

* 直接采用第三方支付宝的SDK，本主键主要扩展异步通知处理
# 仅供测试使用,外部请自行下载alipay java sdk使用
